package com.company;

public class NotPrimitiveInt {
    public int num;

    public NotPrimitiveInt(int i){
        num = i;
    }

    public void Add(){
        num++;
    }

    public int Get(){
        return num;
    }
}
