package com.company;
//import com.joestelmach.natty.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class TextManager {

    public String text;
    private String path;

    private int temp;
    private String word;


    private Map<String, List<String>> levels;

    private List<String> wordsToChange;

    public TextManager(String path, List<String> words) {
        this.path = path;
        temp = 1;
        word = "";
        levels = new HashMap<String, List<String>>();
        wordsToChange = words;
    }

    public void ReadText() throws FileNotFoundException {
        StringBuilder str = new StringBuilder();
        File file = new File(path);
        Scanner scan = new Scanner(file);
        while(scan.hasNextLine()){
            str.append(scan.nextLine() + "\n");
        }
        text = str.toString();
    }

    public String WriteText(){
        return text;
    }

    public String DeleteExtraSpaces(){
        String[] textArr = text.split(" ");
        StringBuilder str = new StringBuilder();
        for (String word: textArr) {
            if(!word.isEmpty()) {
                str.append(word + " ");
            }
        }
        return str.toString();
    }

    public String FindMostRepeatebleWord(){
        String[] textArr = text.split("[\\s.,!?\"]+");
        Map<String,NotPrimitiveInt> map = new HashMap<String,NotPrimitiveInt>();
        for (String word: textArr) {
            String temp = word.toLowerCase();
            if(map.get(temp)!= null) {
                map.get(temp).Add();
            }else{
                map.put(temp,new NotPrimitiveInt(1));
            }
        }
        StringBuilder str = new StringBuilder();
       map.forEach((k,v) -> {if(v.num > temp){temp = v.num; word = k;}});
       temp = 1;
       return word;
    }

    public int SplitOnLevels(int value, String txt, int start){
        int substart = 1;
        int subend = 1;
        for(int i = 1; i< txt.length(); i++){
            if(txt.charAt(i) == '"' && txt.charAt(i-1) == ' '){
                String sentence = txt.substring(i);
                substart = i;
                i = SplitOnLevels(value+1, sentence, i);
                subend = i;
            }else if(txt.charAt(i) == '"' && txt.charAt(i-1) != ' '){
                    if(levels.get(new Integer(value).toString()) != null){
                        String newStr = (txt.substring(1,substart) + txt.substring(subend,i)).replaceAll("\n"," ");
                        levels.get(new Integer(value).toString()).add(newStr);
                    }else{
                        List<String> now = new ArrayList<String>();
                        String newStr = (txt.substring(1,substart) + txt.substring(subend,i)).replaceAll("\n"," ");
                        now.add(newStr);
                        levels.put(new Integer(value).toString(),now);
                    }
                    return i + start + 2;
            }
        }
        return start + txt.length();
    }

    public String PrintLevels(){
        StringBuilder str = new StringBuilder();
        levels.forEach((k,v) -> {str.append("Рівень " + k + "\n"); v.forEach( s -> str.append(s + "\n"));});
        return str.toString();
    }

    public String ReplaceWords(){
        StringBuilder str = new StringBuilder();
        int replaceble = 0;
        int last = 0;
        String replacingWord = FindMostRepeatebleWord();
        for(int i = 1; i< text.length(); i++){
            if(text.charAt(i) == '"' && text.charAt(i-1) == ' '){
                if(replaceble < 1){
                    str.append(text.substring(last,i));
                    last = i;
                }
                replaceble++;
            }else if(text.charAt(i) == '"' && text.charAt(i-1) != ' '){
                replaceble--;
            }
            if(replaceble < 1){
                String temporary = text.substring(last,i);
                for (String word: wordsToChange) {
                    temporary = temporary.replaceAll(word,replacingWord);
                }
                str.append(temporary);
                last = i;
            }
        }
        return  str.toString();
    }

}
