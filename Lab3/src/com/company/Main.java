package com.company;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        List<String> ukrList = new ArrayList<String>();
        ukrList.add("щоб");
        ukrList.add("петро");
        TextManager tm = new TextManager("TextUKR.txt", ukrList);
        tm.ReadText();
        System.out.println(tm.WriteText());
        System.out.println();
        System.out.println(tm.DeleteExtraSpaces());
        System.out.println();
        System.out.println(tm.FindMostRepeatebleWord());
        System.out.println();
        tm.SplitOnLevels(0,tm.text,0);
        System.out.println(tm.PrintLevels());
        System.out.println();
        System.out.println(tm.ReplaceWords());

        System.out.println();

        List<String> engList = new ArrayList<String>();
        engList.add("program");

        TextManager tw = new TextManager("TextENG.txt",engList);
        tw.ReadText();
        System.out.println(tw.WriteText());
        System.out.println();
        System.out.println(tw.DeleteExtraSpaces());
        System.out.println();
        System.out.println(tw.FindMostRepeatebleWord());
        System.out.println();
        tw.SplitOnLevels(0,tw.text,0);
        System.out.println(tw.PrintLevels());
        System.out.println();
        System.out.println(tw.ReplaceWords());

    }
}
