package com.company;

public class Flight {
    private Airplane airplane;
    private Route route;
    private double price;

    public Flight(Airplane airplane, Route route) {
        this.airplane = airplane;
        this.route = route;
        this.price = airplane.getBasePricePerMile() * route.CalculateDistance();
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public double getPrice() {
        return price;
    }
}
