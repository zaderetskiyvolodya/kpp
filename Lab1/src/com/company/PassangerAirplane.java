package com.company;

public class PassangerAirplane implements Airplane
        {
                private String model;
                private int speed;
                private FlightDistance flightDistance;
                private double basePricePerMile;
                private Boolean availibility;
                private int firstClassSeats;
                private int bussinesClassSeats;
                private int economClassSeats;


                public PassangerAirplane(String _model, int _speed, FlightDistance _flightDistance, double _basePricePerMile) {
                        model = _model;
                        speed = _speed;
                        flightDistance = _flightDistance;
                        basePricePerMile = _basePricePerMile;
                }

                public PassangerAirplane(String _model, int _speed, FlightDistance _flightDistance, double _basePricePerMile, int _firstClassSeats, int _bussinesClassSeats, int _economClassSeats) {
                        model = _model;
                        speed = _speed;
                        flightDistance = _flightDistance;
                        basePricePerMile = _basePricePerMile;
                        firstClassSeats = _firstClassSeats;
                        bussinesClassSeats = _bussinesClassSeats;
                        economClassSeats = _economClassSeats;

                }


                public String getModel() {
                        return model;
                }

                public void setModel(String model) {
                        this.model = model;
                }

                public int getSpeed() {
                        return speed;
                }

                public void setSpeed(int speed) {
                        this.speed = speed;
                }

                public FlightDistance getFlightDistance() {
                        return flightDistance;
                }

                public double getBasePricePerMile() {
                        return basePricePerMile;
                }

                public Boolean getAvailibility() {
                        return availibility;
                }

                public void setAvailible() {
                        this.availibility = true;
                }
                public void setUnavailible() {
                        this.availibility = false;
                }

                public int getFirstClassSeats() {
                        return firstClassSeats;
                }

                public void setFirstClassSeats(int firstClassSeats) {
                        this.firstClassSeats = firstClassSeats;
                }

                public int getBussinesClassSeats() {
                        return bussinesClassSeats;
                }

                public void setBussinesClassSeats(int bussinesClassSeats) {
                        this.bussinesClassSeats = bussinesClassSeats;
                }

                public int getEconomClassSeats() {
                        return economClassSeats;
                }

                public void setEconomClassSeats(int economClassSeats) {
                        this.economClassSeats = economClassSeats;
                }
        }
