package com.company;

public class CargoAirplane implements Airplane
{
    private String model;
    private int speed;
    private FlightDistance flightDistance;
    private double basePricePerMile;
    private Boolean availibility;
    private double maxCargoWeight;

    public CargoAirplane(String _model, int _speed, FlightDistance _flightDistance, double _basePricePerMile) {
        model = _model;
        speed = _speed;
        flightDistance = _flightDistance;
        basePricePerMile = _basePricePerMile;
    }

    public CargoAirplane(String _model, int _speed, FlightDistance _flightDistance, double _basePricePerMile, double _maxCargoWeight) {
        model = _model;
        speed = _speed;
        flightDistance = _flightDistance;
        basePricePerMile = _basePricePerMile;
        maxCargoWeight = _maxCargoWeight;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public FlightDistance getFlightDistance() {
        return flightDistance;
    }

    public double getBasePricePerMile() {
        return basePricePerMile;
    }

    public Boolean getAvailibility() {
        return availibility;
    }

    public void setAvailible() {
        this.availibility = true;
    }
    public void setUnavailible() {
        this.availibility = false;
    }

    public double getMaxCargoWeight() {
        return maxCargoWeight;
    }

    public void setMaxCargoWeight(double maxCargoWeight) {
        this.maxCargoWeight = maxCargoWeight;
    }
}
