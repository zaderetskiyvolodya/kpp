package com.company;
import java.lang.String;
import java.lang.Boolean;

public interface Airplane {
    String model = "model";
    int speed = 0;
    FlightDistance flightDistance = FlightDistance.Low;
    double basePricePerMile = 0;
    Boolean availibility = false;


    public String getModel() ;

    public void setModel(String model) ;

    public int getSpeed();

    public void setSpeed(int speed) ;

    public FlightDistance getFlightDistance() ;

    public double getBasePricePerMile() ;

    public Boolean getAvailibility() ;

    public void setAvailible() ;
    public void setUnavailible() ;
}
