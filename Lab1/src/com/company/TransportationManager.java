package com.company;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TransportationManager {
    private List<Airplane> airplanes;
    private List<Flight> flights;

    public Comparator<Flight> FlightPriceComparator = (Flight f1, Flight f2)-> (int)f1.getPrice() - (int)f2.getPrice();
    public static class ModelComparator implements  Comparator<Airplane>{
        @Override
        public  int compare(Airplane a1, Airplane a2){return a1.getModel().compareTo(a2.getModel());}
    }
    public TransportationManager(List<Airplane> airplanes, List<Flight> flights) {
        this.airplanes = airplanes;
        this.flights = flights;
    }

    public Flight ManageFlight(Airport start, Airport destination,Airplane airplane){
        Route route = new Route(start,destination);
        Flight flight = new Flight(airplane,route);
        return  flight;
    }

    public List<Airplane> SortAirplanesByDistanse(){
        Collections.sort(airplanes, new Comparator<Airplane>() {
            @Override
            public int compare(Airplane airplane1, Airplane airplane2) {
                if (airplane1.getFlightDistance() == airplane2.getFlightDistance()) {
                    return airplane1.getModel().compareTo(airplane2.getModel());
                } else {
                    return airplane1.getFlightDistance().compareTo(airplane2.getFlightDistance());
                }
            }
        });
        return airplanes;
    }

    public List<Airplane> SortAirplanesBySpeedRevers(){

            airplanes.sort(new Comparator<Airplane>() {
                @Override
                public int compare(Airplane o1, Airplane o2) {
                    if(o1.getSpeed() == o2.getSpeed()) {
                        return 0;
                    }
                    return o1.getSpeed() < o2.getSpeed() ? 1:-1;
                }
            });
        return airplanes;
    }

    public List<Airplane> SortAirplanesBySpeed(){
       airplanes.sort(Comparator.comparingInt(Airplane::getSpeed));
        return airplanes;
    }

    public List<Flight> SortFlightsByPrice(){
        flights.sort(FlightPriceComparator);
        return flights;
    }

    public  List<Airplane> SortByModel(){
        airplanes.sort(new ModelComparator());
        return airplanes;
    }

    public  void PrintAirplanes(List<Airplane> _airplanes ){
        for (Airplane a: _airplanes
             ) {
            System.out.println(a.getModel() + " " + a.getSpeed() + " " + a.getFlightDistance());
        }
    }

    public  void PrintFlights(List<Flight> _flights){
        for (int i=0; i<_flights.size(); i++) {
            System.out.println(i + " " + flights.get(i).getPrice());
        }
    }
}
