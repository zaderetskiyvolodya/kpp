package com.company;

public class Airport {
    private double latitude;
    private double longtitude;
    private int availablePlaces;

    public Airport(double latitude, double longtitude, int availablePlaces) {
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.availablePlaces = availablePlaces;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }
}
