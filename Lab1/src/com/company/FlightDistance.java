package com.company;

public enum FlightDistance {
    Low,
    Middle,
    SubContinental;
}
