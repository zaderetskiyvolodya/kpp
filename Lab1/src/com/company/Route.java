package com.company;

public class Route {
    private Airport start;
    private Airport destination;
    private final int EarthRadius = 6371000;

    public Route(Airport start, Airport destination) {
        this.start = start;
        this.destination = destination;
    }

    public double CalculateDistance(){
        double φ1 = start.getLatitude() * Math.PI/180;
        double φ2 = destination.getLatitude() * Math.PI/180;
        double Δφ = (destination.getLatitude()-start.getLatitude()) * Math.PI/180;
        double Δλ = (destination.getLongtitude()-destination.getLongtitude()) * Math.PI/180;

        double a = Math.sin(Δφ/2) * Math.sin(Δφ/2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ/2) * Math.sin(Δλ/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = (EarthRadius * c)/1609;

        return distance;
    }

    public Airport getStart() {
        return start;
    }

    public void setStart(Airport start) {
        this.start = start;
    }

    public Airport getDestination() {
        return destination;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }
}
