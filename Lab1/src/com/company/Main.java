package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Airplane> airplanes = new ArrayList<Airplane>();
        airplanes.add(new PassangerAirplane("Boeing", 1150, FlightDistance.SubContinental, 142));
        airplanes.add(new PassangerAirplane("Airbus", 1050, FlightDistance.Middle, 84));
        airplanes.add(new PassangerAirplane("An", 900, FlightDistance.Low, 55));
        airplanes.add(new PassangerAirplane("Lockheed", 920, FlightDistance.SubContinental, 106));
        airplanes.add(new CargoAirplane("Boeing", 1100, FlightDistance.Middle, 102));
        airplanes.add(new CargoAirplane("An", 850, FlightDistance.SubContinental, 145));
        airplanes.add(new CargoAirplane("Airbus", 950, FlightDistance.Low, 78));

        Airport a1 = new Airport(35.213890, -80.943054, 10);
        Airport a2 = new Airport(32.116112, -110.941109, 10);
        Airport a3 = new Airport(51.470020, -0.454295, 10);
        Airport a4 = new Airport(55.865101, -4.433177, 10);

        Route r1 = new Route(a1,a3);
        Route r2 = new Route(a2,a4);
        Route r3 = new Route(a1,a4);
        Route r4 = new Route(a2,a3);

        List<Flight> flights = new ArrayList<>();
        flights.add(new Flight(airplanes.get(0), r1));
        flights.add(new Flight(airplanes.get(3), r2));
        flights.add(new Flight(airplanes.get(2), r3));
        flights.add(new Flight(airplanes.get(5), r4));
        flights.add(new Flight(airplanes.get(4), r3));

        TransportationManager transMan = new TransportationManager(airplanes,flights);

        System.out.println("SortAirplanesByDistanse");
        transMan.PrintAirplanes(transMan.SortAirplanesByDistanse());
        System.out.println();
        System.out.println("SortAirplanesBySpeed");
        transMan.PrintAirplanes(transMan.SortAirplanesBySpeed());
        System.out.println();
        System.out.println("SortAirplanesBySpeedRevers");
        transMan.PrintAirplanes(transMan.SortAirplanesBySpeedRevers());
        System.out.println();
        System.out.println("SortByModel");
        transMan.PrintAirplanes(transMan.SortByModel());
        System.out.println();
        System.out.println("SortFlightsByPrice");
        transMan.PrintFlights(transMan.SortFlightsByPrice());

    }
}
