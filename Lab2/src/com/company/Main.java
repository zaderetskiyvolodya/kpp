package com.company;

public class Main {

    public static void main(String[] args) {
    	String firstFile = "first_workers";
		String secondFile = "second_workers";
	    FileWorker fw = new FileWorker();


	   // fw.WriteToFile(firstFile);
	   // fw.WriteInFile(secondFile);

		fw.ReadFromFileToMap(firstFile);
		System.out.println("Workers from first file:");
		System.out.println(fw.PrintMap());
		fw.SortMapByPositionsToLower();
		System.out.println();
		System.out.println("Sorted by position to Lower");
		System.out.println(fw.PrintMap());
		 fw.SortMapByPositionsToUpper();
		System.out.println();
		System.out.println("Sorted by position to Upper");
		System.out.println(fw.PrintMap());
		 fw.DeleteOlderThanYears(10);
		System.out.println();
		System.out.println("Deleted older than 10 years");
		System.out.println(fw.PrintMap());

		fw.ReadFromTwoFiles(firstFile,secondFile);
		System.out.println();
		System.out.println("Workers from two files without repeats");
		System.out.println(fw.PrintMap());

		System.out.println();
		System.out.println("Positions frequency");
		System.out.println(fw.CountOfPositions());


    }
}
