package com.company;

public class Pair {
    private String surname;
    private Positions position;

    public Pair(String _surname, Positions _position){
        this.surname = _surname;
        this.position = _position;
    }

    public String getSurname() {
        return surname;
    }

    public Positions getPosition() {
        return position;
    }
}
