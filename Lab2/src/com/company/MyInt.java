package com.company;

public class MyInt {
    private int value;

    public MyInt(){
        value = 0;
    }

    public void Add(){
        value++;
    }

    public int getValue() {
        return value;
    }
}
