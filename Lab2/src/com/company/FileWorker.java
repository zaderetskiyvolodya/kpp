package com.company;

import javafx.geometry.Pos;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.*;

public class FileWorker {
    private Map<Date, List<Pair>> workersMap;
    public FileWorker(){
        workersMap = new HashMap<Date, List<Pair>>();

    }

    public void WriteToFile(String _fileName){
        try(FileWriter writer = new FileWriter(_fileName, false))
        {
            writer.write("Zadertskiy Director 25.06.2008\nPavlovich Manager 25.06.2008\nPetrovich Cleaner 15.08.2012\nKavalkovich Administrator 05.10.2016\n" +
                    "Vanenkovich Weighter 25.05.2019\nTudanovich Weighter 25.05.2019\nSudanovna Barmen 25.06.2018\nPanini Chief 25.06.2014\nKurapatkin Cleaner 25.06.2009\n" +
                    "Parmezan Cook 26.11.2008\n");

            writer.close();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    public void WriteInFile(String _fileName){
        try(FileWriter writer = new FileWriter(_fileName, false))
        {

            writer.write("Zadertskiy Director 25.06.2008\nPavlovich Manager 30.06.2008\nSasuke Cleaner 25.02.2019\n" +
                    "Kilishich Weighter 13.05.2017\nMirinish Weighter 13.05.2017\nSudanovna Barmen 25.06.2019\n" +
                    "Kinshiki Cook 22.10.2016\n");

            writer.close();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    public void ReadFromFileToMap(String _fileName){
        try(FileReader reader = new FileReader(_fileName))
        {
            Scanner scan = new Scanner(reader);

            while (scan.hasNextLine()) {
                if(workersMap.isEmpty()){
                    String str = scan.nextLine();
                    String []strBuf = str.split(" ");
                    List<Pair> pairs = new ArrayList<Pair>();
                    pairs.add(new Pair(strBuf[0],Positions.valueOf(strBuf[1])));
                    try{
                        Date date = new SimpleDateFormat("dd.mm.yyyy").parse(strBuf[2]);
                        workersMap.put(date,pairs);
                    }catch(java.text.ParseException e){
                        System.out.println("Cannot write "+ strBuf[0]);
                        e.printStackTrace();
                    }
                   
                }else{
                    String str = scan.nextLine();
                    String []strBuf = str.split(" ");
                    List<Pair> pairs = new ArrayList<Pair>();
                    pairs.add(new Pair(strBuf[0],Positions.valueOf(strBuf[1])));
                    try{
                        Date date = new SimpleDateFormat("dd.mm.yyyy").parse(strBuf[2]);
                        boolean repeted = false;
                        Iterator<Map.Entry<Date, List<Pair>>> it = workersMap.entrySet().iterator();
                        while (it.hasNext()) {
                            if(it.next().getKey().equals(date))
                            {
                                workersMap.get(date).add(pairs.get(0));
                                repeted = true;
                                break;
                            }
                        }
                        if(!repeted){workersMap.put(date,pairs);}
                    }catch(java.text.ParseException e){
                        System.out.println("Cannot write "+ strBuf[0]);
                        e.printStackTrace();
                    }

                }
            }

            reader.close();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    public void ReadFromTwoFiles(String firstFile, String secondFile){
        workersMap.clear();
        try(FileReader reader = new FileReader(firstFile))
        {
            Scanner scan = new Scanner(reader);

            while (scan.hasNextLine()) {
                if(workersMap.isEmpty()){
                    String str = scan.nextLine();
                    String []strBuf = str.split(" ");
                    List<Pair> pairs = new ArrayList<Pair>();
                    pairs.add(new Pair(strBuf[0],Positions.valueOf(strBuf[1])));
                    try{
                        Date date = new SimpleDateFormat("dd.MM.yyyy").parse(strBuf[2]);
                        workersMap.put(date,pairs);
                    }catch(java.text.ParseException e){
                        System.out.println("Cannot write "+ strBuf[0]);
                        e.printStackTrace();
                    }

                }else{
                    String str = scan.nextLine();
                    String []strBuf = str.split(" ");
                    List<Pair> pairs = new ArrayList<Pair>();
                    pairs.add(new Pair(strBuf[0],Positions.valueOf(strBuf[1])));
                    try{
                        Date date = new SimpleDateFormat("dd.mm.yyyy").parse(strBuf[2]);
                        boolean repeted = false;
                        Iterator<Map.Entry<Date, List<Pair>>> it = workersMap.entrySet().iterator();
                        while (it.hasNext()) {
                            if(it.next().getKey().equals(date))
                            {
                                workersMap.get(date).add(pairs.get(0));
                                repeted = true;
                                break;
                            }
                        }
                        if(!repeted){workersMap.put(date,pairs);}
                    }catch(java.text.ParseException e){
                        System.out.println("Cannot write "+ strBuf[0]);
                        e.printStackTrace();
                    }

                }
            }

            reader.close();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
        try(FileReader reader = new FileReader(secondFile))
        {
            Scanner scan = new Scanner(reader);

            while (scan.hasNextLine()) {
                if(workersMap.isEmpty()){
                    String str = scan.nextLine();
                    String []strBuf = str.split(" ");
                    List<Pair> pairs = new ArrayList<Pair>();
                    pairs.add(new Pair(strBuf[0],Positions.valueOf(strBuf[1])));
                    try{
                        Date date = new SimpleDateFormat("dd.mm.yyyy").parse(strBuf[2]);
                        workersMap.put(date,pairs);
                    }catch(java.text.ParseException e){
                        System.out.println("Cannot write "+ strBuf[0]);
                        e.printStackTrace();
                    }

                }else{
                    String str = scan.nextLine();
                    String []strBuf = str.split(" ");
                    List<Pair> pairs = new ArrayList<Pair>();
                    pairs.add(new Pair(strBuf[0],Positions.valueOf(strBuf[1])));
                    try{
                        Date date = new SimpleDateFormat("dd.mm.yyyy").parse(strBuf[2]);
                        boolean repeted = false;
                        boolean hasSurname = false;
                        Iterator<Map.Entry<Date, List<Pair>>> it = workersMap.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry<Date,List<Pair>> p = it.next();
                            if(p.getKey().equals(date))
                            {
                                repeted = true;
                            }
                            for (Pair par:p.getValue())
                            {
                                if(par.getSurname().equals(pairs.get(0).getSurname()))
                                {
                                    hasSurname = true;
                                    break;
                                }
                            }
                        }
                        if(!repeted && !hasSurname){workersMap.put(date,pairs);}
                        else if(repeted && !hasSurname){
                            workersMap.get(date).add(pairs.get(0));
                        }
                    }catch(java.text.ParseException e){
                        System.out.println("Cannot write "+ strBuf[0]);
                        e.printStackTrace();
                    }

                }
            }

            reader.close();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

    }

    public void SortMapByPositionsToUpper(){
        Iterator<Map.Entry<Date, List<Pair>>> it = workersMap.entrySet().iterator();
        while (it.hasNext()) {
           it.next().getValue().sort(new Comparator<Pair>() {
               @Override
               public int compare(Pair o1, Pair o2) {
                   if(o1.getPosition().ordinal()<o2.getPosition().ordinal()){
                       return 1;
                   }else if(o1.getPosition().ordinal()>o2.getPosition().ordinal()){
                       return -1;
                   }
                   return 0;
               }
           });
        }
    }

    public void SortMapByPositionsToLower(){
        Iterator<Map.Entry<Date, List<Pair>>> it = workersMap.entrySet().iterator();
        while (it.hasNext()) {
            it.next().getValue().sort(new Comparator<Pair>() {
                @Override
                public int compare(Pair o1, Pair o2) {
                    if(o1.getPosition().ordinal()>o2.getPosition().ordinal()){
                        return 1;
                    }else if(o1.getPosition().ordinal()<o2.getPosition().ordinal()){
                        return -1;
                    }
                    return 0;
                }
            });
        }
    }

    public void DeleteOlderThanYears(int _years){
        Iterator<Map.Entry<Date, List<Pair>>> its = workersMap.entrySet().iterator();
        Map<Date, List<Pair>> map = new HashMap<Date, List<Pair>>();
        while (its.hasNext()) {
            Map.Entry<Date, List<Pair>> pair = its.next();
            map.put(pair.getKey(),pair.getValue());
        }
        Iterator<Map.Entry<Date, List<Pair>>> it = map.entrySet().iterator();
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        date = cal.getTime();
        cal.add(Calendar.YEAR, -1*_years);
        date = cal.getTime();
        while (it.hasNext()) {
            Map.Entry<Date, List<Pair>> pair = it.next();
             if(pair.getKey().compareTo(date) < 0){
                workersMap.remove(pair.getKey(), pair.getValue());
            }
        }
    }

    public StringBuilder CountOfPositions(){
        Map<Positions,MyInt> positions = new HashMap<>();
        for (Positions pos:Positions.values()) {
            positions.put(pos,new MyInt());
        }
        Iterator<Map.Entry<Date, List<Pair>>> it = workersMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Date, List<Pair>> p = it.next();
            for (Pair par:p.getValue()) {
                positions.get(par.getPosition()).Add();
            }
        }

        StringBuilder values = new StringBuilder();
        Iterator<Map.Entry<Positions,MyInt>> itr = positions.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<Positions, MyInt> p = itr.next();
            values.append(new String(p.getKey().toString() + " : " + p.getValue().getValue() + "\n"));
        }
        return values;
    }

    public String PrintMap(){
        String str = new String();
        Iterator<Map.Entry<Date, List<Pair>>> it = workersMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Date, List<Pair>> pair = it.next();
            str += pair.getKey().toString() + " :  ";
            for (Pair st: pair.getValue())
            {
                str += st.getSurname() + " - " + st.getPosition().toString() + "\n";
            }
        }
        return str;
    }
}
