package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

public class MyRunnable implements Runnable {

    public ObservableList<String> list;
    public String name;

    MyRunnable(String _name, ListView<String> tf){

        name = _name;
    }

    @Override
    public void run(){
        list = FXCollections.observableArrayList();
        System.out.println("Starting " + name);
        list.add("Starting " + name);
        try {
            for (int i = 4; i > 0; i--) {
                list.add("Thread: " + name + ", " + i);
                System.out.println("Thread: " + name + ", " + i);
                // Let the thread sleep for a while.
                Thread.sleep(250);
            }
        } catch (InterruptedException e) {
            list.add("Thread " + name + " interrupted.");
            System.out.println("Thread " + name + " interrupted.");
        }
        list.add("Thread " + name + " finishing.");
        System.out.println("Thread " + name + " finishing.");
    }
}
