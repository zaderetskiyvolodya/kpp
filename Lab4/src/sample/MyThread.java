package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

public class MyThread extends Thread{
    public Thread t;
    public ObservableList<String> list;
    ListView<String> textArea = null;

    MyThread(String name, ListView<String> tf){

        super(name);
        textArea = tf;
    }

    @Override
    public  void run() {
        list = FXCollections.observableArrayList();
        System.out.println("Starting " + getName());
        list.add("Starting " + getName());
        try {
            for (int i = 4; i > 0; i--) {
                list.add("Thread: " + getName() + ", " + i);
                System.out.println("Thread: " + getName() + ", " + i);
                // Let the thread sleep for a while.
                Thread.sleep(250);
            }
        } catch (InterruptedException e) {
            list.add("Thread " + getName() + " interrupted.");
            System.out.println("Thread " + getName() + " interrupted.");
        }
        list.add("Thread " + getName() + " finishing.");
        System.out.println("Thread " + getName() + " finishing.");

    }

}
