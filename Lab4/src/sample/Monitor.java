package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Monitor extends Thread{
    List<MyThread> tList;
    ListView<String> textArea = null;
    public ObservableList<String> list;
    ListView<String> textArea2 = null;

    Monitor(List<MyThread> tList, ListView<String> tf, ListView<String> tf2){
        this.textArea = tf;
        this.tList = tList;
        this.textArea2 = tf2;
    }


    @Override
    public void run() {
        list = FXCollections.observableArrayList();
        list.clear();
        ObservableList<String> list2 = FXCollections.observableArrayList();
        for (MyThread th: tList) {
            th.start();
            try {
                th.join();
            } catch (InterruptedException e) {
                list.add("Thread " + getName() + " interrupted.");
                System.out.println("Thread " + getName() + " interrupted.");
            }
            list.addAll(th.list);
        }

    }

}
