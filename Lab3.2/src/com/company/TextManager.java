package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextManager {

    private String text;
    private int length;

    public TextManager(String fileName) throws FileNotFoundException {
        length = 4;
        text = ReadFromFile(fileName);
    }

    public TextManager(int length,String txt){
        this.length = length;
        text = txt;
    }

    public String ReadFromFile(String filename) throws FileNotFoundException {
        File txt = new File(filename);

        Scanner scnr = new Scanner(txt);
        StringBuilder str = new StringBuilder();
        while(scnr.hasNextLine()) {
            str.append(scnr.nextLine() + "\n");
        }
        return str.toString();
    }

    public String[] SplitOnSentences(){
        String[] sentences = text.split("[.!]+");
        return sentences;
    }

    public String SeparateQuestions(String str){
        String string = "";
        if(str.indexOf('?')!=-1) {
            string = str.substring(0, str.indexOf('?'));
        }
        if(str.indexOf('?') != str.length()-1 && str.substring(str.indexOf('?')+1,str.length()).indexOf('?')!=-1)  {
            string += "//" + SeparateQuestions(str.substring(str.indexOf('?')+1,str.length()));
        }
            return string;
    }

    public List<String> FindQuestionSentences(){
        String[] sen = SplitOnSentences();
        List<String> questions = new ArrayList<String>();
        for (String st:sen) {
            String[] s = SeparateQuestions(st).split("//");
            for (String stri: s) {
                questions.add(stri);
            }

        }
        return  questions;
    }

    public String PrintQuestions(){
        List<String> que = FindQuestionSentences();
        StringBuilder str = new StringBuilder();
        que.forEach(st -> str.append(st+"\n"));
        return str.toString();
    }

    public String FindWords(){
        String[] strArr = PrintQuestions().split("[, ]+") ;
        List<String> words = new ArrayList<>();
        boolean m = true;
        for (String str: strArr) {
            if(str.length() == length){
                for (String s:words) {
                    if(s.equals(str)){
                        m = false;
                    }
                }
                if(m) {
                    words.add(str);
                }
                m = true;
            }
        }
        StringBuilder str = new StringBuilder();
        for (String s:words) {
            str.append(s + "\n");
        }
        return str.toString();
    }
}
